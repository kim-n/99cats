module SessionHelper
  def current_user
    @session ||= Session.find_by_token(session[:session_token])
    @current_user ||= @session.nil? ? nil : @session.user
  end

  def login_user!(user)
    @current_user = user
    @session = user.sessions.create(user_id: user.id, user_agent: request.env["HTTP_USER_AGENT"], ip_address: request.remote_ip)
    session[:session_token] = @session.token
  end
end
