class Session < ActiveRecord::Base
  attr_accessible :ip_address, :user_agent, :user_id, :token

  validates :ip_address, :user_agent, :user_id, :token, presence: true

  before_validation :set_token

  belongs_to(
    :user,
    class_name: 'User',
    foreign_key: :user_id,
    primary_key: :id
  )
  def set_token
    self.token ||= generate_token
  end

  def generate_token
    SecureRandom.base64
  end

  def self.delete_session_by_token(token)
    sess = Session.find_by_token(token)
    sess.destroy unless sess.nil?

    nil
  end

  def location
    geo = Geocoder.search(self.ip_address).first
    "#{geo.city}, #{geo.state_code}, #{geo.country_code}"
  end
end
