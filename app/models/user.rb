require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :user_name, :password_digest, :session_token


  validates :user_name, :password_digest, :session_token, presence: true
  validates :user_name, :session_token, uniqueness: true

  has_many(
    :cats,
    class_name: "Cat",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
    :sessions,
    class_name: 'Session',
    foreign_key: :user_id,
    primary_key: :id
  )

  before_validation :set_session_token

  def self.find_by_credentials(user_name, pwd)
    user = User.find_by_user_name(user_name)
    user if user.nil? || user.is_password?(pwd)
  end

  def password=(pwd)
    self.password_digest = BCrypt::Password.create(pwd)
  end

  def is_password?(pwd)
    BCrypt::Password.new(self.password_digest).is_password?(pwd)
  end
end
