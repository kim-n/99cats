class CatRentalRequest < ActiveRecord::Base
  attr_accessible :cat_id, :start_date, :end_date, :status

  validates :cat_id, :start_date, :end_date, :status, :presence => true
  validates :cat_id, numericality: true
  validates :status, inclusion: { in: %w(PENDING APPROVED DENIED) }
  validate :no_overlapping_approved_requests

  before_validation :pending_status

  belongs_to(
    :cat,
    class_name: "Cat",
    foreign_key: :cat_id,
    primary_key: :id
  )

  def overlapping_requests
    range = [start_date..end_date]
    cat_requests = CatRentalRequest.where(cat_id: self.cat_id)
    start_overlaps = cat_requests.where(start_date: range)
    end_overlaps = cat_requests.where(end_date: range)

    start_overlaps + end_overlaps
  end

  def overlapping_approved_requests
    overlapping_requests.select { |req| req.status == "APPROVED" }
  end

  def no_overlapping_approved_requests
    unless overlapping_approved_requests.empty?
      errors[:base] << "That cat is already reserved"
    end
  end

  def pending_status
    self.status ||= "PENDING"
  end

  def overlapping_pending_requests
    overlapping_requests.select { |req| req.status == 'PENDING' }
  end

  def approve!
    transaction do
      self.status = 'APPROVED'
      self.save!
      overlapping_pending_requests.each do |req|
        req.deny! unless req.id = self.id
      end
    end
  end

  def deny!
    self.status = "DENIED"
    self.save!
  end

  def pending?
    self.status == 'PENDING'
  end
end
