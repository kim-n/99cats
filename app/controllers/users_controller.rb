class UsersController < ApplicationController

  def new

  end

  def create
    user = User.new
    user.user_name = params[:user][:user_name]
    user.password = params[:password]
    user.save
    login_user!(user)
    redirect_to cats_url
  end

  def show
    @user = current_user
    if @user.nil?
      redirect_to cats_url
    end
  end
end
