class CatsController < ApplicationController
  before_filter :check_owner, only: [:edit, :update]
  before_filter :logged_in, only: [:edit, :update]
  def index
    @cats = Cat.all
  end

  def show
    @cat = Cat.find(params[:id])
  end

  def new
    @cat = Cat.new
  end

  def create
    @cat = Cat.new(params[:cat])
    @cat.user_id = current_user.id
    if @cat.save
      redirect_to cat_path(@cat)
    else
      render :text => "Error"
    end
  end

  def edit
    @cat = Cat.find(params[:id])
  end

  def update
    @cat = Cat.find(params[:id])
    if @cat.update_attributes(params[:cat])
      redirect_to cat_path(@cat)
    else
      render 'edit'
    end
  end

  def check_owner
    @cat = Cat.find(params[:id])
    redirect_to cats_url unless @cat.user_id == current_user.id
  end

  def logged_in
    redirect_to cats_url unless current_user
  end
end
