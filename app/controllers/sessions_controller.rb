class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_credentials(params[:session][:user_name], params[:session][:password])
    if user.nil?
      render :json => "Credentials were wrong"
    else
      login_user!(user)
      redirect_to cats_url
    end
  end

  def destroy
    if current_user
      Session.delete_session_by_token(session[:session_token])
      session[:session_token] = nil
    end
    redirect_to cats_url
  end

  def logout
    sess = Session.find(params[:id])
    if sess.user_id == current_user.id
      if sess.token == session[:session_token]
        session[:session_token] = nil
      end
      sess.destroy
    end
    redirect_to user_path(current_user)
  end
end
