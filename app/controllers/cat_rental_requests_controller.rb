class CatRentalRequestsController < ApplicationController
  before_filter :check_owner, only: [:approve, :deny]

  def new
  end

  def create
    rental = CatRentalRequest.new(params[:cat_rental_request])
    if rental.save
      redirect_to cat_path(rental.cat)
    else
      render text: "ERROR"
    end
  end

  def approve
    rental = CatRentalRequest.find(params[:id])
    rental.approve!
    redirect_to rental.cat
  end

  def deny
    rental = CatRentalRequest.find(params[:id])
    rental.deny!
    redirect_to rental.cat
  end

  def check_owner
    rental = CatRentalRequest.find(params[:id])
    redirect_to cats_url unless rental.cat.user_id == current_user.id
  end
end
