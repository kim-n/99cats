class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.integer :user_id, null: false
      t.string :ip_address, null: false
      t.string :user_agent, null: false
      t.string :token, null: false

      t.timestamps
    end

    remove_column :users, :session_token, :string
  end
end
