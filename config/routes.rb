NinetyNineCats::Application.routes.draw do
  resources :cats
  resources :cat_rental_requests, only: [:new, :create] do
    member do
      post 'approve'
      post 'deny'
    end
  end
  resources :users, only: [:new, :create, :show]
  resource :session
  delete 'logout/:id', to: "Sessions#logout", as: :logout
end
